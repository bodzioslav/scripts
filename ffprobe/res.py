#!/usr/bin/env python3

import os 
import mimetypes
import subprocess
import json
import argparse

def ffprobe(videoAbsPath):
    try:
      ffprobeResult = subprocess.run(["ffprobe", "-v", "error",
        "-select_streams", "v:0", "-show_entries", "stream=width,height",
        "-of", "json", videoAbsPath], check=True, timeout=5,
        universal_newlines=True, capture_output=True)
    except subprocess.CalledProcessError as e:
      e.stdout
    except subprocess.TimeoutExpired as e:
      e.stdout
    except FileNotFoundError:
      exit("ffprobe not found. Please install it and try again.")

    return json.loads(ffprobeResult.stdout)

def checkRes(videoAbsPath):
    ffprobeJson = ffprobe(videoAbsPath)
    
    ffprobeWidth = ffprobeJson['streams'][0]['width']
    ffprobeHeight = ffprobeJson['streams'][0]['height']

    return ffprobeWidth, ffprobeHeight

def checkFiles(videoDir, videoWidth, videoHeight, reverseCondition=False):
    
    # mp4, mkv, avi, wmv, webm
    validMimetypes = ['video/mp4', 'video/x-matroska', 'video/x-msvideo',
      'video/x-ms-wmv', 'video/webm']
    fileList = []
    
    for dirpath, dirs, files in os.walk(videoDir):
      for file in files:
        fileMimetype = mimetypes.MimeTypes().guess_type(file)[0]
        fileAbsPath = os.path.join(dirpath, file)
        if fileMimetype in validMimetypes:
          ffprobeWidth, ffprobeHeight = checkRes(fileAbsPath)
          if reverseCondition is False:
            if ffprobeWidth >= videoWidth and ffprobeHeight >= videoHeight:
              fileList.append(fileAbsPath)
          else:
            if ffprobeWidth < videoWidth and ffprobeHeight < videoHeight:
              fileList.append(fileAbsPath)

    return fileList

if __name__ == "__main__":
    opts = argparse.ArgumentParser()

    opts.add_argument("-d", "--dir", 
      help="Specify directory to scan (default is CWD)")
    opts.add_argument("--width", 
      help="Specify video width to compare against (default is 1280)")
    opts.add_argument("--height", 
      help="Specify video height to compare against (default is 720)")
    opts.add_argument("-r", "--reverse", 
      help="""List videos with lower reslution than provided,
      by default (width < 1280 and height < 720)""",
      action="store_true")

    args = opts.parse_args()

    if args.dir:
        videoDir = args.dir
    else:
        videoDir = os.getcwd()

    if args.width and args.height:
        videoWidth = int(args.width)
        videoHeight = int(args.height)
    else:
        videoWidth = int(1280)
        videoHeight = int(720)

    if args.reverse:
      checkedFiles = checkFiles(videoDir, videoWidth, videoHeight,
      reverseCondition = True)
    else:
      checkedFiles = checkFiles(videoDir, videoWidth, videoHeight)

    print('\n'.join(['"{}"'.format(f) for f in checkedFiles]))