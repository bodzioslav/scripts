#!/usr/bin/env python3

import requests
import json
import argparse

# https://github.com/transmission/transmission/blob/main/docs/rpc-spec.md
def transmission_auth(url, user, password, auth_header):
    try:
        auth_page = requests.get(url, auth=(user, password))
        if auth_page.status_code == requests.codes.CONFLICT:
            auth_token = auth_page.headers[auth_header]
            return auth_token
        else:
            exit("Transmission RPC: Unauthirized " + str(auth_page.status_code))
    except requests.exceptions.ConnectionError:
        exit("Transmission RPC: Connection Error")
    except requests.exceptions.Timeout:
        exit("Transmission RPC: Conenction Timeout")


# https://github.com/transmission/transmission/blob/main/docs/rpc-spec.md
def transmission_get_torrents(url, user, password, auth_header, auth_token):
    transmission_post_data = {
        "arguments": {
            "fields": [
                "name",
                "files",
                "downloadDir",
                "trackers",
                "id",
                "error",
                "errorString",
            ]
        },
        "method": "torrent-get",
    }
    try:
        request_torrents = requests.post(
            url,
            auth=(user, password),
            headers={auth_header: auth_token},
            json=transmission_post_data,
        )
        if request_torrents.status_code == requests.codes.OK:
            transmission_torrents = request_torrents.json()
            return transmission_torrents["arguments"]["torrents"]
        else:
            exit("Transmission RPC: Unauthorized " + str(request_torrents.status_code))
    except requests.exceptions.ConnectionError:
        exit("Transmission RPC: Connection Error")
    except requests.exceptions.Timeout:
        exit("Transmission RPC: Conenction Timeout")


# get rid of unnecessary torrent keys
def filter_torrent_keys(torrent, allowed_keys):
    values = []

    for key in allowed_keys:
        values.append(torrent[key])

    return dict(zip(allowed_keys, values))


# filter torrents list based on allowed torrent keys
def filter_torrents_list(torrents_list, allowed_keys):
    modified_torrents_list = []

    for torrent in torrents_list:
        modified_torrents_list.append(filter_torrent_keys(torrent, allowed_keys))

    return modified_torrents_list


# find duplicated torrents with the same ['name', 'files', 'downloadDir'] keys
def find_duplicated_torrents(torrents_list):
    torrent_keys = ["name", "files", "downloadDir"]
    modified_torrents_list = filter_torrents_list(torrents_list, torrent_keys)
    duplicated_torrents = []

    for torrent in modified_torrents_list:
        # if torrent with the same name, files, filesizes and downloadsDir appears more than once
        if modified_torrents_list.count(torrent) > 1:
            duplicated_torrents.append(torrent)

    return duplicated_torrents


# filter out duplicated torrents from the full torrents list with all available keys
def get_duplicated_torrents(torrents_list, duplicated_torrents_list):
    duplicated_torrents_names = [
        torrent["name"] for torrent in duplicated_torrents_list
    ]
    duplicated_torrents_verbose_list = []

    for torrent in torrents_list:
        if torrent["name"] in duplicated_torrents_names:
            duplicated_torrents_verbose_list.append(torrent)

    return duplicated_torrents_verbose_list


# check if torrent is from specified tracker
def filter_torrent_tracker(torrent, tracker_name):
    for tracker in torrent["trackers"]:
        if tracker_name in tracker["announce"] or tracker_name in tracker["scrape"]:
            return True
    return False


# filter torrents by tracker
def filter_by_tracker(torrents_list, tracker_name):
    filtered_torrents = []
    for torrent in torrents_list:
        if filter_torrent_tracker(torrent, tracker_name):
            filtered_torrents.append(torrent)

    return filtered_torrents


# remove torrents by id without removing the downloaded files
def transmission_remove_torrents(
    url, user, password, auth_header, auth_token, torrents_list
):
    torrent_ids = [torrent["id"] for torrent in torrents_list]
    torrent_names = [torrent["name"] for torrent in torrents_list]
    transmission_post_data = {
        "arguments": {"ids": torrent_ids, "delete-local-data": "false"},
        "method": "torrent-remove",
    }

    try:
        remove_torrents = requests.post(
            url,
            auth=(user, password),
            headers={auth_header: auth_token},
            json=transmission_post_data,
        )
        if remove_torrents.status_code == requests.codes.OK:
            print("Torrents Removed:", json.dumps(torrent_names, indent=4), sep="\n")
        return remove_torrents.json()
    except requests.exceptions.ConnectionError:
        exit("Transmission RPC: Connection Error")
    except requests.exceptions.Timeout:
        exit("Transmission RPC: Conenction Timeout")


def script_runtime():
    # defaults
    transmission_auth_header = "X-Transmission-Session-Id"
    default_transmission_url = "http://127.0.0.1:9091/transmission/rpc"

    script_options = argparse.ArgumentParser()

    script_options.add_argument(
        "-u",
        "--username",
        type=str,
        required=True,
        help="Username for Transmission RPC",
    )
    script_options.add_argument(
        "-p",
        "--password",
        type=str,
        required=True,
        help="Password for Transmission RPC",
    )
    script_options.add_argument(
        "--url",
        type=str,
        default=default_transmission_url,
        help="URL for Transmission RPC",
    )

    script_subparsers = script_options.add_subparsers(title="commands", dest="command")

    dupes_subparser = script_subparsers.add_parser(
        "dupes",
        description="Command to manage duplicated (cross seeded) torrents",
        add_help=True,
        help="duplicates -h",
    )

    dupes_options = dupes_subparser.add_mutually_exclusive_group(required=True)

    dupes_subparser.add_argument(
        "--tracker",
        type=str,
        help="Filter duplicated torrents by tracker",
    )

    dupes_options.add_argument(
        "-l",
        "--list",
        help="List duplicated torrents (detailed info)",
        action="store_true",
    )

    dupes_options.add_argument(
        "-n",
        "--names",
        help="List duplicated torrents (names only)",
        action="store_true",
    )

    dupes_options.add_argument(
        "-r",
        "--remove",
        help="List duplicated torrents names and remove them, used with --tracker",
        action="store_true",
    )

    script_args = script_options.parse_args()

    transmission_auth_token = transmission_auth(
        script_args.url,
        script_args.username,
        script_args.password,
        transmission_auth_header,
    )

    transmission_torrents_list = transmission_get_torrents(
        script_args.url,
        script_args.username,
        script_args.password,
        transmission_auth_header,
        transmission_auth_token,
    )

    if script_args.command == "dupes":
        found_duplicated_torrents = find_duplicated_torrents(transmission_torrents_list)
        duplicated_torrents_list = get_duplicated_torrents(
            transmission_torrents_list, found_duplicated_torrents
        )

        if script_args.tracker:
            filtered_duplicated_torrents = filter_by_tracker(
                duplicated_torrents_list, script_args.tracker
            )

        if script_args.list and script_args.tracker:
            print(json.dumps(filtered_duplicated_torrents, indent=4))
        elif script_args.list:
            print(json.dumps(duplicated_torrents_list, indent=4))

        if script_args.names and script_args.tracker:
            duplicated_torrents_names_list = [
                torrent["name"] for torrent in filtered_duplicated_torrents
            ]
            duplicated_torrents_names_list.sort()
            print(json.dumps(duplicated_torrents_names_list, indent=4))
        elif script_args.names:
            duplicated_torrents_names_list = [
                torrent["name"] for torrent in duplicated_torrents_list
            ]
            duplicated_torrents_names_list.sort()
            print(json.dumps(duplicated_torrents_names_list, indent=4))

        if script_args.remove and script_args.tracker:
            remove_cmd_result = transmission_remove_torrents(
                script_args.url,
                script_args.username,
                script_args.password,
                transmission_auth_header,
                transmission_auth_token,
                filtered_duplicated_torrents,
            )
            print(remove_cmd_result)

        elif script_args.remove and not script_args.tracker:
            print("Use --tracker with --remove option!")


if __name__ == "__main__":
    script_runtime()
